﻿//подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;
const int W = 800, H = 600; //Ширина и Высота окна

int main()
{

    // Создание окна с определенными размерами и названием
    sf::RenderWindow window(sf::VideoMode(W, H), L"Название на русском!");

    // Круг
    float radius = 100.f;
    sf::CircleShape shape1(radius, 8);
    // Задание точки начала координат для фигуры
    shape1.setOrigin(100, 100);
    // Задание цвета фигуре
    sf::Uint8 r = 236, g = 159, b = 38, a = 255;
    shape1.setFillColor(sf::Color{r, g, b, a});
    // Задание позиции фигуре
    int x1 = radius, y1 = radius;
    shape1.setPosition(x1, y1);

    // Прямоугольник
    float width = 100.f, length = 100.f;
    sf::RectangleShape shape2(sf::Vector2f(width, length));
    shape2.setFillColor(sf::Color{ 80, 200, 155, 255 });
    shape2.setOrigin(50, 50);
    int x2 = 2 * radius + 100, y2 = y1;
    shape2.setPosition(x2, y2);

    // Треугольник (правильный)
    float R = 80.f; //радиус описанной окружности
    sf::CircleShape shape3(R, 3);
    shape3.setFillColor(sf::Color{ 150, 50, 150, 255 });
    shape3.setOrigin(80, 80);
    int x3 = x2 + (width / 2) + 100, y3 = y1;
    shape3.setPosition(x3, y3);

    while (window.isOpen())
    {   //переменная для хранения события
        sf::Event event;
        //цикл по всем событиям
        while (window.pollEvent(event))
        {   //обработка события
            //если нажат крестик, то
            if (event.type == (sf::Event::Closed))
                //закрыть окно
                window.close();
        }

        // Условия остановки
        if (y1 <= (H - radius)) {
            y1++;
            //r++; g++; b++;
            //shape1.setFillColor(sf::Color{ r, g, b, a });
        }
        shape1.setPosition(x1, y1);

        if (y2 <= H - (length / 2)) {
            y2 += 3;
            //shape2.rotate(5.f);
        }
        shape2.setPosition(x2, y2);

        if (y3 <= H - (R / 2)) {  // (R/2) радиус вписанной окружности
            y3 += 2;
            //shape3.rotate(10.f);
        }
        shape3.setPosition(x3, y3);

        // Очистить окно
        window.clear();
        window.draw(shape1);
        window.draw(shape2);
        window.draw(shape3);
        // Отобразить на окне все что есть в "буфере"
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    return 0;
}